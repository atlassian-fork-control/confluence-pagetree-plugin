/*
 * Copyright (c), Shannon Krebs
 * Refer to bundled license.txt file for license information
 */

package com.atlassian.confluence.plugins.pagetree;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Macro for displaying pagetreesearch input form on a page.
 *
 * @author Shannon Krebs
 * @since 1.0
 */
public class PageTreeSearchMacro extends BaseMacro implements Macro {
    private static final Logger log = LoggerFactory.getLogger(PageTreeSearchMacro.class);

    public static final String ROOTPAGE_PARAM = "rootPage";
    public static final String ROOTPAGE_PARAM_ALIAS = "rootpage";

    private PageManager pageManager;
    private SpaceManager spaceManager;
    private SettingsManager settingsManager;
    private WebResourceManager webResourceManager;

    public String execute(Map paramaters, String body, RenderContext renderContext) {
        return execute(paramaters, body, new DefaultConversionContext(renderContext));
    }

    public RenderMode getBodyRenderMode() {
        return RenderMode.NO_RENDER;
    }

    public boolean hasBody() {
        return false;
    }

    public boolean isInline() {
        return false;
    }

    public PageManager getPageManager() {
        return pageManager;
    }

    public void setPageManager(PageManager pageManager) {
        this.pageManager = pageManager;
    }

    public SpaceManager getSpaceManager() {
        return spaceManager;
    }

    public void setSpaceManager(SpaceManager spaceManager) {
        this.spaceManager = spaceManager;
    }

    public void setSettingsManager(SettingsManager settingsManager) {
        this.settingsManager = settingsManager;
    }

    public void setWebResourceManager(WebResourceManager webResourceManager) {
        this.webResourceManager = webResourceManager;
    }

    ///CLOVER:OFF
    protected String getRenderedTemplateWithoutSwallowingErrors(
            Map velocityContext) throws Exception {
        return VelocityUtils.getRenderedTemplateWithoutSwallowingErrors(
                "vm/search.vm", velocityContext);
    }

    protected Map<String, Object> getDefaultVelocityContext() {
        return MacroUtils.defaultVelocityContext();
    }
///CLOVER:OFF

    public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) {
        List<String> errors = new ArrayList<String>();

        String rootPageString = parameters.get(ROOTPAGE_PARAM);

        // Try the alias
        if (rootPageString == null) {
            rootPageString = parameters.get(ROOTPAGE_PARAM_ALIAS);
        }
        // Still null try the first un-named parameter
        if (rootPageString == null) {
            rootPageString = parameters.get("0");
        }

        Page rootPage = null;
        String spaceKey = null;
        String pageName = null;

        if (rootPageString != null) {
            // Try to find the page/space the user has specified
            int delimeterPos = rootPageString.indexOf(":");
            if (delimeterPos > -1) {
                spaceKey = rootPageString.substring(0, delimeterPos);
            }
            if (rootPageString.length() > delimeterPos) {
                pageName = rootPageString.substring(delimeterPos + 1);
            }

            if (StringUtils.isEmpty(spaceKey)) {
                // Space key is null, use the current space
                spaceKey = conversionContext.getSpaceKey();
            } else {
                // check that the space exists
                if (spaceManager.getSpace(spaceKey) == null) {
                    errors.add("pagetreesearch.rootspace.notfound");
                }
            }

            if (!StringUtils.isEmpty(pageName)) {
                rootPage = pageManager.getPage(spaceKey, pageName);

                if (rootPage == null) {
                    errors.add("pagetreesearch.rootpage.notfound");
                }
            }
        } else {
            // Root page not specified, use the current context
            ContentEntityObject contentObject = conversionContext.getEntity();
            if (contentObject instanceof Page) {
                spaceKey = conversionContext.getSpaceKey();
                rootPage = (Page) contentObject;
            } else {
                errors.add("pagetreesearch.rootpage.invalidpage");
            }
        }

        Map velocityContext = getDefaultVelocityContext();
        velocityContext.put(ROOTPAGE_PARAM, rootPage);
        velocityContext.put("spaceKey", spaceKey);
        velocityContext.put("errors", errors);
        velocityContext.put("outputType", conversionContext.getOutputType());
        velocityContext.put("baseUrl", settingsManager.getGlobalSettings().getBaseUrl());

        boolean mobile = false;
        if ("mobile".equals(conversionContext.getOutputDeviceType())) {
            webResourceManager.requireResourcesForContext("atl.confluence.plugins.pagetree-mobile");
            mobile = true;
        } else {
            webResourceManager.requireResourcesForContext("atl.confluence.plugins.pagetree-desktop");
        }

        velocityContext.put("mobile", mobile);

        try {
            return getRenderedTemplateWithoutSwallowingErrors(velocityContext);
        } catch (Exception e) {
            log.error("Error happened", e);
        }

        return "";
    }

    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }
}
