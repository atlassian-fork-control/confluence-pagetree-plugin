/*
 * Copyright (c), Shannon Krebs
 * Refer to bundled license.txt file for license information
 */

package com.atlassian.confluence.plugins.pagetree;

import com.atlassian.bonnie.Searchable;
import com.atlassian.bonnie.search.Extractor;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.Page;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implements the extractor interface to add page hierarchy information to the
 * Confluence search index.
 *
 * @author Shannon Krebs
 * @since 1.0
 */
public class PageHierarchyExtractor implements Extractor {
    private static final Logger log = LoggerFactory.getLogger(PageHierarchyExtractor.class);

    /**
     * The field id that is used to store the ancestor IDs
     */
    public static final String ANCESTORS_KEY = "ancestorIds";
    public static final String POSITION_KEY = "position";
    public static final int UNSPECIFIED_POSITION = -1;

    /**
     * Adds the ancestor field information to the given document. This extractor
     * will only apply to searchable objects that are instances of a Page.
     *
     * @param document              the lucene document to add the ancestorIds field to
     * @param defaultSearchableText not used in this extractor
     * @param searchable            the searchable object, this extractor will only add the
     *                              ancestorIds field for Page objects.
     */
    public void addFields(Document document, StringBuffer defaultSearchableText, Searchable searchable) {
        Page page = null;

        if (searchable instanceof Page) {
            page = (Page) searchable;
        } else if (searchable instanceof Attachment) {
            Attachment attachment = (Attachment) searchable;
            ContentEntityObject ceo = attachment.getContainer();
            if (ceo instanceof Page) {
                page = (Page) ceo;
            }
        }

        if (page != null) {
            // Add the current page to the list so the search will find itself
            document.add(new StringField(ANCESTORS_KEY, String.valueOf(page.getId()), Field.Store.YES));

            // Add the rest of the pages that are parents of the current page
            for (Page element : page.getAncestors()) {
                document.add(new StringField(ANCESTORS_KEY, String.valueOf(element.getId()), Field.Store.YES));
            }

            if (searchable instanceof Page) {
                // We need this position to build page tree properly
                int position = (page.getPosition() != null) ? page.getPosition() : UNSPECIFIED_POSITION;
                document.add(new StringField(POSITION_KEY, String.valueOf(position), Field.Store.YES));
            }

            if (log.isDebugEnabled()) {
                log.debug("searchable:  " + searchable + " page: " + page.getTitle() + " ancestors: " + page.getAncestors());
            }
        }
    }
}
