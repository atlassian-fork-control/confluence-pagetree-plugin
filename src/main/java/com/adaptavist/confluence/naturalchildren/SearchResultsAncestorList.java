package com.adaptavist.confluence.naturalchildren;

/**
 * Implementation of AncestorList which parses ancestors from search index, and keeps them in properly order
 * For example, we have such page tree configuration:
 * root page (id = 1)
 * |- child page (id = 2)
 * . |- grand child page (id = 3)
 * . . |- great grand parent (id = 4)
 *
 * <p>
 * But SearchManager will return ancestors in this order:
 * great grand parent (id = 4)
 * root page (id = 1)
 * child page (id = 2)
 * grand child page (id = 3)
 * <p>
 * The current (last) page is always put in the beginning
 * This class eliminates this issue
 *
 * @since 4.0.0
 */
class SearchResultsAncestorList extends AncestorList {
    SearchResultsAncestorList(String[] ancestors) {
        for (int i = 1; i < ancestors.length; i++) {
            addAncestor(Long.parseLong(ancestors[i]));
        }
        if (ancestors.length > 0) {
            addAncestor(Long.parseLong(ancestors[0]));
        }
    }
}
