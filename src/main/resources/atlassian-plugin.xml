<!--
Copyright (c) 2005, BNP Paribas
All rights reserved.

By william jones & zohar melamed

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
Neither the name of the BNP Paribas nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

<atlassian-plugin name='${project.name}' key='${atlassian.plugin.key}'>
    <plugin-info>
        <description>${project.description}</description>
        <vendor name="Atlassian" url="http://www.atlassian.com"/>
        <version>${project.version}</version>
    </plugin-info>

    <resource type="velocity" name="pagetree" location="templates/extra/tree/tree.vm"/>
    <resource type="i18n" name="propertyfile" location="com.atlassian.confluence.plugins.pagetree.i18n" />

    <web-resource key="pagetree-js-resources" name="Pagetree JS Resources">
        <transformation extension="js">
            <transformer key="jsI18n"/>
        </transformation>
        <transformation extension="soy">
            <transformer key="soyTransformer"/>
        </transformation>

        <resource type="download" name="pagetree-soy.js" location="soy/pagetree.soy"/>
        <resource type="download" name="pagetree.js" location="com/atlassian/confluence/plugins/pagetree/resource/javascript/pagetree.js" />
        <resource type="download" name="pagetree-desktop.js" location="com/atlassian/confluence/plugins/pagetree/resource/javascript/pagetree-desktop.js" />
        <dependency>${atlassian.plugin.key}:pagetree-css-resources</dependency>
        <dependency>confluence.web.resources:jquery</dependency>
        <context>atl.confluence.plugins.pagetree-desktop</context>
    </web-resource>

    <web-resource key="pagetree-mobile-resources" name="Mobile Pagetree Resources">
        <resource type="download" name="pagetree-mobile.js" location="com/atlassian/confluence/plugins/pagetree/resource/javascript/pagetree-mobile.js"/>
        <resource type="download" name="pagetree.js" location="com/atlassian/confluence/plugins/pagetree/resource/javascript/pagetree.js"/>
        <resource type="download" name="pagetree-mobile.css" location="com/atlassian/confluence/plugins/pagetree/resource/css/pagetree-mobile.css"/>
        <dependency>${atlassian.plugin.key}:pagetree-css-resources</dependency>
        <context>atl.confluence.plugins.pagetree-mobile</context>
    </web-resource>

    <web-resource key="pagetree-css-resources" name="Pagetree CSS Resources">
        <resource type="download" name="pagetree.css" location="com/atlassian/confluence/plugins/pagetree/resource/css/pagetree.css"/>
        <context>main</context>
    </web-resource>

    <extractor name="Page Hierarchy Extractor" key="PageHierarchyExtractor"
               class="com.atlassian.confluence.plugins.pagetree.PageHierarchyExtractor" priority="900">
       <description>Adds page hierachy information to the lucene index so you can search within a given page tree.</description>
   </extractor>

    <macro name='pagetree' class='com.bnpparibas.confluence.extra.tree.PageTree' key='pagetree'>
        <description key="pagetree.description"/>
        <resource type="velocity" name="help" location="com/atlassian/confluence/plugins/pagetree/pagetree-help.vm">
            <param name="help-section" value="confluence"/>
        </resource>
    </macro>

    <macro name='pagetreesearch' class='com.atlassian.confluence.plugins.pagetree.PageTreeSearchMacro' key='pagetreesearch'>
        <description key="pagetreesearch.description"/>
        <resource type="velocity" name="help" location="com/atlassian/confluence/plugins/pagetree/pagetreesearch-help.vm">
            <param name="help-section" value="confluence"/>
        </resource>
    </macro>

    <xhtml-macro name='pagetree' class='com.bnpparibas.confluence.extra.tree.PageTree' key='pagetree-xhtml'>
        <description key="pagetree.description"/>
        <category name="navigation"/>
        <parameters>
            <parameter name="root" type="confluence-content">
                <option key="type" value="page"/>
            </parameter>
            <parameter name="spaces" type="spacekey">
                <alias name="space"/>
            </parameter>
            <parameter name="sort" type="enum" default="position">
                <value name="bitwise"/>
                <value name="creation"/>
                <value name="modified"/>
                <value name="natural"/>
                <value name="position"/>
            </parameter>
            <parameter name="excerpt" type="boolean"/>
            <parameter name="reverse" type="boolean"/>
            <parameter name="searchBox" type="boolean"/>
            <parameter name="expandCollapseAll" type="boolean"/>
            <parameter name="startDepth" type="int" default="1"/>
        </parameters>
        <device-type>mobile</device-type>
    </xhtml-macro>

    <xhtml-macro name='pagetreesearch' class='com.atlassian.confluence.plugins.pagetree.PageTreeSearchMacro' key='pagetreesearch-xhtml'>
        <description key="pagetreesearch.description"/>
        <category name="navigation"/>
        <device-type>mobile</device-type>
    </xhtml-macro>

    <xwork name="naturalchildrenaction" key="naturalchildrenaction">
        <package name="naturalchildren" extends="default" namespace="/plugins/pagetree">
            <default-interceptor-ref name="defaultStack" />
            <action name="naturalchildren" class="com.adaptavist.confluence.naturalchildren.NaturalChildrenAction" method="doDefault">
                <result name="input" type="velocity">/vm/children.vm</result>
                <result name="error" type="httpheader">
                   <param name="status">403</param>
                </result>
                <result name="success" type="velocity">/vm/children.vm</result>
            </action>
        </package>
    </xwork>

    <xwork name="Pagetree Search Actions" key="pagetreesearchactions">
        <description>Webwork actions for Pagetree Search Plugin</description>

        <package name="pagetreesearchactions" extends="default" namespace="/plugins/pagetreesearch">
            <default-interceptor-ref name="defaultStack" />

             <action name="pagetreesearch"
                class="com.atlassian.confluence.plugins.pagetree.PageTreeSearchAction">
                <result name="search" type="redirect">${searchActionString}</result>
                <resource type="i18n" name="i18nPropertyFile" location="com.atlassian.confluence.plugins.pagetree.i18n" />
            </action>

        </package>
    </xwork>
</atlassian-plugin>