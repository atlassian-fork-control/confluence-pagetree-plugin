package it.com.atlassian.confluence.plugins.pagetree;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public abstract class AbstractConfluencePluginWebTestCaseBase extends AbstractConfluencePluginWebTestCase
{
    public AbstractConfluencePluginWebTestCaseBase()
    {
        super();
    }

    protected long createSpace(String spaceKey, String title, String description)
    {
        SpaceHelper spaceHelper = getSpaceHelper();
        spaceHelper.setKey(spaceKey);
        spaceHelper.setName(title);
        spaceHelper.setDescription(description);
        spaceHelper.setHomePageId(55555);
        try
        {
            spaceHelper.create();
        }
        catch(Exception e)
        {
           // not doing anything here as space is already created
        }

        return spaceHelper.getHomePageId();
    }

    protected void deleteSpace(String spaceKey)
    {
        SpaceHelper spaceHelper = getSpaceHelper(spaceKey);
        assertTrue(spaceHelper.delete());
    }

    protected long createPage(String spaceKey, String title, String content, long parentId)
    {
        return createPage(spaceKey, parentId, title, content, Collections.emptyList());
    }

    protected long createPage(String spaceKey, String title, String content)
    {
        return createPage(spaceKey, 0, title, content, Collections.emptyList());
    }

    protected long createPage(String spaceKey, long parentId, String title, String content, List labels)
    {
        PageHelper helper = getPageHelper();

        helper.setSpaceKey(spaceKey);
        helper.setParentId(parentId);
        helper.setTitle(title);
        helper.setContent(content);
        helper.setCreationDate(new Date());
        helper.setLabels(labels);
        assertTrue(String.format("Failed to create page [%s] in space %s with parent %s", title, spaceKey, parentId), helper.create());

        // return the generated id for the new page
        return helper.getId();
    }

    protected void viewPageById(long entityId)
    {
        gotoPage("/pages/viewpage.action?pageId=" + entityId);
    }

    protected void createPageInSpaceHomePage(String spaceKey, String spaceTitle,String title, String content)
    {
    	gotoPage("/display/" + spaceKey + "/" + spaceTitle +"+Home");

    	String homePageId = getElementAttributeByXPath("//meta[@name='ajs-page-id']", "content");
    	
    	createPage(spaceKey, Long.parseLong(homePageId),title, content, Collections.emptyList());
    }

    protected void setConfluenceBaseUrl(String baseUrl)
    {
        try
        {
            gotoPageWithEscalatedPrivileges("/admin/editgeneralconfig.action");
            setTextField("domainName", baseUrl);
            submit();
        }
        finally
        {
            dropEscalatedPrivileges();
        }
    }

    protected File copyClasspathResourceToTempFile(String classPathResource, String prefix, String suffix)
            throws IOException
    {

        File tempFile = File.createTempFile(prefix, suffix);
        try(
                InputStream in = getClass().getClassLoader().getResourceAsStream(classPathResource);
                OutputStream out = new FileOutputStream(tempFile)
        ) {
            IOUtils.copy(in, out);
            return tempFile;
        }
    }
}
