package com.adaptavist.confluence.naturalchildren;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class AncestorListTestCase {
    @Test
    public void checkDuplicatesAreRemoved() {
        // use the sequence: "1", "2", "1", "2", "100"
        // but we have to provide "100", "1", "2", "1", "2"
        // keep in mind that the current page will be put in the beginning
        // see {@link com.adaptavist.confluence.naturalchildren.SearchResultsAncestorList} for more information
        final String[] originalAncestorsWithDuplicates = new String[]{"100", "1", "2", "1", "2"};
        SearchResultsAncestorList searchResultsAncestorList = new SearchResultsAncestorList(originalAncestorsWithDuplicates);

        final Set<Long> expectedAncestorsWithoutDuplicates = new LinkedHashSet<>(Arrays.asList(1L, 2L, 100L));
        assertThat(searchResultsAncestorList.ancestors, is(expectedAncestorsWithoutDuplicates));
    }

    @Test
    public void checkAncestorsWithoutDuplicates() {
        final String[] originalAncestorsWithDuplicates = new String[]{"100", "1", "2", "3"};
        SearchResultsAncestorList searchResultsAncestorList = new SearchResultsAncestorList(originalAncestorsWithDuplicates);

        final Set<Long> expectedAncestorsWithoutDuplicates = new LinkedHashSet<>(Arrays.asList(1L, 2L, 3L, 100L));
        assertThat(searchResultsAncestorList.ancestors, is(expectedAncestorsWithoutDuplicates));
    }

    @Test
    public void checkRootPage() {
        final String[] originalAncestorsWithDuplicates = new String[]{"100"};
        SearchResultsAncestorList searchResultsAncestorList = new SearchResultsAncestorList(originalAncestorsWithDuplicates);

        final Set<Long> expectedAncestorsWithoutDuplicates = Collections.singleton(100L);
        assertThat(searchResultsAncestorList.ancestors, is(expectedAncestorsWithoutDuplicates));
    }

    /**
     * checkNoAncestors - the idea is just to check that it is not crashed
     */
    @Test
    public void checkNoAncestors() {
        final String[] originalAncestorsWithDuplicates = new String[]{};
        SearchResultsAncestorList searchResultsAncestorList = new SearchResultsAncestorList(originalAncestorsWithDuplicates);

        final Set<Long> expectedAncestorsWithoutDuplicates = Collections.emptySet();
        assertThat(searchResultsAncestorList.ancestors, is(expectedAncestorsWithoutDuplicates));
    }
}
